# Checkout service

This service offers promotion management functionality. 

The service is configured to listen on port **8088**. Swagger documentation is available in
[http://localhost:8088/swagger-ui.html](http://localhost:8088/swagger-ui.html)

## Requirements

For building and running the application you need:

- [JDK 11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `Application` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Promotions
During basket checkout, promotions are applied and, depending on basket contents and product configuration, discounts are offered.

The list of promotions that are available, including the order with which they are applied, can be found in `PromotionConfig` class. Promotion behavior can be altered by changing properties in `application.yml`.
