package com.client.checkout.domain.product;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

/**
 * Object that carries information about Products.
 * Created by Dimitris Charilas on 06/10/21.
 */
@Data
@Builder
public class Product {

  private String id;
  private String name;
  private BigDecimal price;

}
