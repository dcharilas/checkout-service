package com.client.checkout.interceptor;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.isNull;


import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.lang.NonNull;
import org.springframework.web.util.ContentCachingResponseWrapper;

/**
 * Custom filter that logs all incoming requests/responses.
 * Created by Dimitris Charilas on 07/10/21.
 */
@Log4j2
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HttpLogger implements Filter {

  @Builder.Default
  private List<Pattern> uriIgnorePatterns = List.of(
      /*
       * Actuator Related
       */
      Pattern.compile("/actuator.*"),
      /*
       * Swagger Related
       */
      Pattern.compile("/"),
      Pattern.compile("/csrf"),
      Pattern.compile("/swagger.*"),
      Pattern.compile("/webjars.*"),
      Pattern.compile("/v2/api-docs.*")
  );

  private static String getBodyFromBytes(byte[] bytes) {
    return isNull(bytes) ? null : new String(bytes, UTF_8).replaceAll("\\r?\\n", "");
  }

  @Override
  @SneakyThrows
  public void doFilter(@NonNull ServletRequest servletRequest, @NonNull ServletResponse servletResponse, @NonNull FilterChain chain) {
    var request = (HttpServletRequest) servletRequest;
    var response = (HttpServletResponse) servletResponse;

    MultiReadHttpServletRequest requestCacheWrapper = new MultiReadHttpServletRequest(request);
    ContentCachingResponseWrapper responseCacheWrapper = new ContentCachingResponseWrapper(response);
    boolean ignoreCompletely = shouldIgnore(requestCacheWrapper, uriIgnorePatterns);

    if (!ignoreCompletely) {
      if (!isNull(request.getCharacterEncoding())) {
        log.info("REQUEST: " + requestCacheWrapper.getRequestURI() + " - PAYLOAD: " +
            requestCacheWrapper.getReader().lines().collect(Collectors.joining()).replaceAll("\\r?\\n", ""));
      }
    }
    chain.doFilter(requestCacheWrapper, responseCacheWrapper);
    if (!ignoreCompletely) {
      log.info("RESPONSE: " + requestCacheWrapper.getRequestURI() + " - PAYLOAD: " +
          getBodyFromBytes(responseCacheWrapper.getContentAsByteArray()));
    }
    responseCacheWrapper.copyBodyToResponse();
  }

  private boolean shouldIgnore(MultiReadHttpServletRequest logRequest, List<Pattern> ignorePatterns) {
    return !isNull(ignorePatterns)
        && ignorePatterns
        .stream()
        .anyMatch(e -> e.matcher(logRequest.getRequestURI()).matches());
  }

}
