package com.client.checkout.exception;

/**
 * Exception thrown in case of non existing product id.
 * Created by Dimitris Charilas on 06/10/21.
 */
public class InvalidProductException extends Exception {

  public InvalidProductException() {
  }

  public InvalidProductException(final String message) {
    super(message);
  }

}
