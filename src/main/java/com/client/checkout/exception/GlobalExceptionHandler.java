package com.client.checkout.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


import com.client.checkout.model.api.ErrorDetails;
import com.client.checkout.model.api.ErrorResponse;
import com.client.checkout.model.api.GenericResponse;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

/**
 * Global handler for exceptions. GenericResponse object is always returned and in case of exceptions,
 * ErrorResponse obejct is filled with applied exceptions.
 * Created by Dimitris Charilas on 06/10/21.
 */
@Log4j2
@ControllerAdvice
public class GlobalExceptionHandler {

  @ResponseBody
  @ResponseStatus(BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public GenericResponse<Object> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex, WebRequest request) {
    var debugMessage = ex.getBindingResult().getFieldErrors()
        .stream()
        .map(e -> String.format("'%s':'%s'", e.getField(), e.getDefaultMessage()))
        .collect(Collectors.joining(", "));
    var requestDescription = request.getDescription(false);
    log.warn("Bad Request: {} | Debug: {}", requestDescription, debugMessage);

    return GenericResponse.builder()
        .exceptions(
            ErrorResponse.builder()
                .errors(List.of(
                    ErrorDetails.builder()
                        .title(MethodArgumentNotValidException.class.getSimpleName())
                        .httpStatus(BAD_REQUEST.value())
                        .timestamp(ZonedDateTime.now())
                        .build()))
                .build())
        .build();
  }

  @ResponseBody
  @ResponseStatus(INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public GenericResponse<Object> handleException(Exception exception, WebRequest request) {
    if (exception instanceof NullPointerException) {
      log.error(exception.getMessage(), exception);
    } else {
      log.error(exception.getMessage());
      exception.printStackTrace();
    }
    var body = GenericResponse.builder()
        .exceptions(
            ErrorResponse.builder()
                .errors(List.of(
                    ErrorDetails.builder()
                        .httpStatus(INTERNAL_SERVER_ERROR.value())
                        .title(exception.getMessage())
                        .timestamp(ZonedDateTime.now())
                        .build()))
                .build())
        .build();
    return body;
  }
}
