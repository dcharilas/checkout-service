package com.client.checkout.service;

import com.client.checkout.domain.product.Product;
import com.client.checkout.model.CheckoutItem;
import com.client.checkout.model.CheckoutResponse;
import com.client.checkout.model.basket.Basket;
import com.client.checkout.promotion.Promotion;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * Created by Dimitris Charilas on 06/10/21.
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class CheckoutService {

  private final ProductService productService;
  private final List<Promotion> promotions;

  /**
   * Performs checkout for basket. After product definitions are retrieved, promotions are applied and finally
   * total cost is calculated.
   *
   * @param basket the basket containing products selected by the user
   * @return the CheckoutResponse, containing all product prices and total cost
   */
  public CheckoutResponse checkout(Basket basket) {
    Map<String, Product> products =
        productService.getProductDefinitions(
            basket.getItems().stream().map(i -> i.getId()).collect(Collectors.toSet()));

    CheckoutResponse response = CheckoutResponse.builder()
        .items(basket.getItems().stream()
            .map(i -> new CheckoutItem(i, products.get(i.getId())))
            .collect(Collectors.toList()))
        .build();

    /* All active promotions are applied and discount are accumulated within CheckoutResponse */
    applyPromotions(response);

    response.setTotalCost(calculateTotalCost(response));
    response.setCurrency(basket.getCurrency());
    return response;
  }

  /**
   * Applies all existing promotions to basket. Discounts are stored within CheckoutResponse.
   *
   * @param response the CheckoutResponse, containing all product prices and total cost
   */
  private void applyPromotions(CheckoutResponse response) {
    for (Promotion promotion : promotions) {
      promotion.apply(response);
    }
  }

  /**
   * Calculates total cost as follows
   * - sums prices of each product, multiplied by quantity
   * - subtracts accumulated absolute discounts
   * - applies accumulated % discount
   * Precision is set to 2 decimals
   *
   * @param response the CheckoutResponse, containing all product prices and total cost
   * @return the calculated total cost of the basket (BigDecimal)
   */
  private BigDecimal calculateTotalCost(CheckoutResponse response) {
    return response.getItems().stream()
        .map(i -> i.getPrice().multiply(BigDecimal.valueOf(i.getQuantity())))
        .reduce(BigDecimal.ZERO, BigDecimal::add)
        .subtract(response.getAbsoluteDiscount())
        .multiply((BigDecimal.valueOf(100).subtract(response.getDiscountPercentage())).divide(BigDecimal.valueOf(100)))
        .setScale(2, RoundingMode.DOWN);
  }
}
