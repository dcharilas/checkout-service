package com.client.checkout.service;

import static java.util.Objects.isNull;


import com.client.checkout.domain.product.Product;
import com.client.checkout.exception.InvalidProductException;
import com.client.checkout.repository.ProductRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * Operations related to products.
 * Created by Dimitris Charilas on 06/10/21.
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class ProductService {

  private final ProductRepository productRepository;

  /**
   * Returns product definitions from Product Catalogue. In case a requested product does not exist,
   * InvalidProductException is thrown
   *
   * @param productIds the list of product ids for which definitions must be loaded
   * @return
   */
  public Map<String, Product> getProductDefinitions(Set<String> productIds) {
    if (isNull(productIds) || productIds.isEmpty()) {
      return new HashMap<>();
    }
    List<Product> products = productRepository.getProductDefinitions(productIds);
    validateProductIds(productIds, products);
    return products.stream().collect(Collectors.toMap(Product::getId, Function.identity()));
  }

  /**
   * Validates that all requested product ids exist in Product Catalogue. Throws exception in case validation fails.
   *
   * @param productIds the list of product ids requested
   * @param products   the list of retrieved product definitions
   * @throws InvalidProductException
   */
  @SneakyThrows
  private void validateProductIds(Set<String> productIds, List<Product> products) {
    for (String id : productIds) {
      if (products.stream().filter(p -> id.equalsIgnoreCase(p.getId())).count() == 0) {
        throw new InvalidProductException(String.format("Product id %s does not exist", id));
      }
    }
  }

}
