package com.client.checkout.controller;

import static com.client.checkout.model.api.GenericResponse.fromData;


import com.client.checkout.model.CheckoutResponse;
import com.client.checkout.model.api.GenericResponse;
import com.client.checkout.model.basket.Basket;
import com.client.checkout.service.CheckoutService;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller that performs basket operations.
 * Created by Dimitris Charilas on 06/10/21.
 */
@Log4j2
@RestController
@RequestMapping("/basket")
@RequiredArgsConstructor
public class BasketController {

  private final CheckoutService checkoutService;

  @ApiOperation("Operation that handles basket checkout. Applies promotions and price per item, as well as total cost.")
  @PostMapping(value = "/checkout", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public GenericResponse<CheckoutResponse> checkoutBasket(@RequestBody @Valid Basket basket) {
    return fromData(checkoutService.checkout(basket));
  }
}
