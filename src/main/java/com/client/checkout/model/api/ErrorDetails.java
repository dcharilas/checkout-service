package com.client.checkout.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonPropertyOrder( {
    "code",
    "title",
    "datetime"
})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDetails {

  @JsonProperty("code")
  private int httpStatus;

  @JsonProperty("title")
  private String title;

  @JsonProperty("datetime")
  private ZonedDateTime timestamp;
}
