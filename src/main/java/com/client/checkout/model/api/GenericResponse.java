package com.client.checkout.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonPropertyOrder( {
    "data",
    "exceptions"
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponse<T> {

  @JsonProperty("data")
  protected T data;

  @JsonProperty("exceptions")
  @Builder.Default
  protected ErrorResponse exceptions = ErrorResponse.builder().build();

  public static <T> GenericResponse<T> fromData(T data) {
    return GenericResponse.<T>builder()
        .data(data)
        .build();
  }
}
