package com.client.checkout.model;

import static java.util.Objects.isNull;


import com.client.checkout.model.constant.Currency;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Dimitris Charilas on 06/10/21.
 */
@Data
@Builder
public class CheckoutResponse {

  @ApiModelProperty(value = "List of items included in checkout")
  private List<CheckoutItem> items;
  @ApiModelProperty(value = "The total cost of the basket")
  private BigDecimal totalCost;
  @ApiModelProperty(value = "The currency of total cost")
  private Currency currency;

  /* Fields used only for internal processing */
  @Builder.Default
  @JsonIgnore
  private BigDecimal discountPercentage = BigDecimal.ZERO;
  @Builder.Default
  @JsonIgnore
  private BigDecimal absoluteDiscount = BigDecimal.ZERO;
  @JsonIgnore
  private List<String> appliedDiscounts;

  /* Util methods */
  public void addAppliedDiscount(String discount) {
    if (isNull(appliedDiscounts)) {
      appliedDiscounts = new LinkedList<>();
    }
    appliedDiscounts.add(discount);
  }

  public void addAbsoluteDiscount(BigDecimal discount) {
    absoluteDiscount = absoluteDiscount.add(discount);
  }

  public void addDiscountPercentage(BigDecimal discount) {
    discountPercentage = discountPercentage.add(discount);
  }
}
