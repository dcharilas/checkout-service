package com.client.checkout.model.basket;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Dimitris Charilas on 06/10/21.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BasketItem {

  @NotNull
  @NotBlank
  @ApiModelProperty(value = "Product id of the selected item", required = true)
  private String id;

  @Positive
  @ApiModelProperty(value = "Quantity of the selected item (must be a positive number)", required = true)
  private int quantity;
}
