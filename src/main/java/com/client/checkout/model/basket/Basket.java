package com.client.checkout.model.basket;

import com.client.checkout.model.constant.Currency;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Dimitris Charilas on 06/10/21.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Basket {

  @NotNull
  @ApiModelProperty(value = "List of items contained in the Basket", required = true)
  private List<@Valid BasketItem> items;

  @ApiModelProperty(value = "The currency to be used")
  @Builder.Default
  private Currency currency = Currency.BRITISH_POUND;
}
