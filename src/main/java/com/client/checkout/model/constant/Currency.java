package com.client.checkout.model.constant;

/**
 * Enumeration of supported currencies.
 * Created by Dimitris Charilas on 06/10/21.
 */
public enum Currency {

  EURO, BRITISH_POUND;
}
