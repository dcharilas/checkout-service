package com.client.checkout.model;

import com.client.checkout.domain.product.Product;
import com.client.checkout.model.basket.BasketItem;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Dimitris Charilas on 06/10/21.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckoutItem extends BasketItem {

  @ApiModelProperty(value = "The product name")
  private String name;
  @ApiModelProperty(value = "The product price")
  private BigDecimal price;

  public CheckoutItem(BasketItem basketItem, Product product) {
    this.setId(basketItem.getId());
    this.setQuantity(basketItem.getQuantity());
    this.setName(product.getName());
    this.setPrice(product.getPrice());
  }

}
