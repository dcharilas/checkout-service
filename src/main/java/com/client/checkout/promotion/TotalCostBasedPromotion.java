package com.client.checkout.promotion;

import com.client.checkout.config.promotion.CostDiscountProperties;
import com.client.checkout.model.CheckoutResponse;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;

/**
 * Promotion that is based on the total calculated cost (without other promotions being applied).
 * Created by Dimitris Charilas on 06/10/21.
 */
@Log4j2
public class TotalCostBasedPromotion implements Promotion {

  private static boolean active;
  private List<CostDiscountProperties.CostPromotionConfig> discounts;

  public TotalCostBasedPromotion(CostDiscountProperties costDiscountProperties) {
    this.discounts = costDiscountProperties.getDiscounts().stream()
        .sorted(Comparator.comparingDouble(CostDiscountProperties.CostPromotionConfig::getMinimumCost).reversed())
        .collect(Collectors.toList());
    this.active = costDiscountProperties.isActive();
  }

  /**
   * Applies the discount strategy.
   *
   * @param response the CheckoutResponse, containing all product prices and total cost
   */
  @Override
  public void apply(CheckoutResponse response) {
    if (!active) {
      log.info("Skipping promotion : " + this.getClass().getName());
      return;
    }
    log.info("Applying promotion : " + this.getClass().getName());
    Optional<CostDiscountProperties.CostPromotionConfig> discount = getCostPromotion(response);
    if (discount.isPresent()) {
      log.info("Adding discount percentage : " + discount.get().getDiscountPercentage());
      response.addDiscountPercentage(BigDecimal.valueOf(discount.get().getDiscountPercentage()));
      response.addAppliedDiscount(this.getClass().getName());
    }
  }

  /**
   * Calculates total cost of products, before any discount is applied.
   *
   * @param response the CheckoutResponse, containing all product prices and total cost
   * @return the total cost of the included products
   */
  private BigDecimal calculateTotalCostBeforeDiscounts(CheckoutResponse response) {
    //TODO it is assumed that this promotion does not take into account already applied discounts
    return response.getItems().stream()
        .map(i -> i.getPrice().multiply(BigDecimal.valueOf(i.getQuantity())))
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  /**
   * Finds the proper discount that can be applied based on total cost. Discounts are ordered
   * in descending total cost, so the first eligible one is applied.
   *
   * @param response the CheckoutResponse, containing all product prices and total cost
   * @return the discount configuration to be applied (if any exists)
   */
  private Optional<CostDiscountProperties.CostPromotionConfig> getCostPromotion(CheckoutResponse response) {
    BigDecimal cost = calculateTotalCostBeforeDiscounts(response);
    return discounts.stream()
        .filter(d -> cost.compareTo(BigDecimal.valueOf(d.getMinimumCost())) > 0)
        .findFirst();
  }
}
