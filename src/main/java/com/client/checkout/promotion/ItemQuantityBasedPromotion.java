package com.client.checkout.promotion;

import static java.util.Objects.isNull;


import com.client.checkout.config.promotion.QuantityDiscountProperties;
import com.client.checkout.model.CheckoutItem;
import com.client.checkout.model.CheckoutResponse;
import java.math.BigDecimal;
import java.util.List;
import lombok.extern.log4j.Log4j2;

/**
 * Promotion based on the quantity of specific product. If more or equal products are found compared to the
 * designated quantity, then the discount amount is subtracted for each product contained in the basket.
 * Created by Dimitris Charilas on 06/10/21.
 */
@Log4j2
public class ItemQuantityBasedPromotion implements Promotion {

  private static boolean active;
  private List<QuantityDiscountProperties.QuantityPromotionConfig> discounts;

  public ItemQuantityBasedPromotion(QuantityDiscountProperties quantityDiscountProperties) {
    this.discounts = quantityDiscountProperties.getDiscounts();
    this.active = quantityDiscountProperties.isActive();
  }

  /**
   * Applies the discount strategy.
   *
   * @param response the CheckoutResponse, containing all product prices and total cost
   */
  @Override
  public void apply(CheckoutResponse response) {
    if (!active || isNull(discounts)) {
      log.info("Skipping promotion : " + this.getClass().getName());
      return;
    }
    log.info("Applying promotion : " + this.getClass().getName());
    for (QuantityDiscountProperties.QuantityPromotionConfig discount : discounts) {
      int quantity = countQuantity(response.getItems(), discount.getProductId());
      if (quantity >= discount.getMinimumQuantity()) {
        BigDecimal discountToAdd = BigDecimal.valueOf(discount.getDiscount()).multiply(BigDecimal.valueOf(quantity));
        log.info("Adding absolute discount : " + discountToAdd);
        response.addAbsoluteDiscount(discountToAdd);
        response.addAppliedDiscount(this.getClass().getName());
      }
    }
  }

  /**
   * Counts how many times a product appears in the basket.
   *
   * @param items     the list of items included in the basket
   * @param productId the product id to search for
   * @return the number of times the requested product id is included in the configuration
   */
  private int countQuantity(List<CheckoutItem> items, String productId) {
    return items.stream()
        .filter(i -> productId.equalsIgnoreCase(i.getId()))
        .map(i -> i.getQuantity())
        .reduce(0, Integer::sum);
  }
}
