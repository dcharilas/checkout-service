package com.client.checkout.promotion;

import com.client.checkout.model.CheckoutResponse;

/**
 * Generic interface that all promotions should implement.
 * Created by Dimitris Charilas on 06/10/21.
 */
public interface Promotion {

  void apply(CheckoutResponse response);
}
