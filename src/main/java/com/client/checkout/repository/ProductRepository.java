package com.client.checkout.repository;

import com.client.checkout.domain.product.Product;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;

/**
 * Repository to handle product information.
 * Created by Dimitris Charilas on 09/10/21.
 */
@Repository
public class ProductRepository {

  /**
   * Returns product information.
   *
   * @param productIds the list of product ids for which definitions must be loaded
   * @return the list of retrieved product definitions
   */
  public List<Product> getProductDefinitions(Set<String> productIds) {
    return List.of(
            Product.builder().id("0001").name("Water Bottle").price(BigDecimal.valueOf(24.95)).build(),
            Product.builder().id("0002").name("Hoodie").price(BigDecimal.valueOf(65.00)).build(),
            Product.builder().id("0003").name("Sticker Set").price(BigDecimal.valueOf(3.99)).build())
        .stream()
        .filter(p -> productIds.contains(p.getId()))
        .collect(Collectors.toList());
  }

}
