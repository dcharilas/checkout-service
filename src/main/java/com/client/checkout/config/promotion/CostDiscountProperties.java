package com.client.checkout.config.promotion;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Properties related to TotalCostBasedPromotion.
 * Created by Dimitris Charilas on 06/10/21.
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "promotion.total-cost-based")
@Configuration
public class CostDiscountProperties {

  private List<CostDiscountProperties.CostPromotionConfig> discounts = new ArrayList<>();
  private boolean active;


  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class CostPromotionConfig {

    private Double discountPercentage;
    private Double minimumCost;

  }

}
