package com.client.checkout.config.promotion;

import com.client.checkout.promotion.ItemQuantityBasedPromotion;
import com.client.checkout.promotion.TotalCostBasedPromotion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * Configuration used to determined which promotions are applied. Optionally execution order can be defined.
 * Created by Dimitris Charilas on 09/10/21.
 */
@Configuration
public class PromotionConfig {

  @Autowired
  private QuantityDiscountProperties quantityDiscountProperties;

  @Autowired
  private CostDiscountProperties costDiscountProperties;

  @Order(2)
  @Bean
  public ItemQuantityBasedPromotion getItemQuantityBasedPromotion() {
    return new ItemQuantityBasedPromotion(quantityDiscountProperties);
  }

  @Order(1)
  @Bean
  public TotalCostBasedPromotion getTotalCostBasedPromotion() {
    return new TotalCostBasedPromotion(costDiscountProperties);
  }
}
