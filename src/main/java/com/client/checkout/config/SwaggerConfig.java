package com.client.checkout.config;

import javax.servlet.ServletContext;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configuration related to Swagger.
 * Created by Dimitris Charilas on 07/10/21.
 */
@Configuration
@EnableSwagger2
@AllArgsConstructor
public class SwaggerConfig implements WebMvcConfigurer {

  private final Environment environment;
  private final ServletContext servletContext;

  @Bean
  public Docket api() {
    var docket = new Docket(DocumentationType.SWAGGER_2)
        .pathProvider(new RelativePathProvider(servletContext) {
          @Override
          public String getApplicationBasePath() {
            return environment.getProperty("server.servlet.context-path");
          }
        })
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .build();
    return docket;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/swagger-ui.html**")
        .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
    registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
  }
}
