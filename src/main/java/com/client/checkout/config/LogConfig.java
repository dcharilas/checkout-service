package com.client.checkout.config;

import com.client.checkout.interceptor.HttpLogger;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CompositeFilter;

/**
 * Configuration related to logging. Custom HttpLogger is added in order to ensure all incoming requests/responses
 * are automatically logged.
 * Created by Dimitris Charilas on 07/10/21.
 */
@Configuration
public class LogConfig {

  @Bean
  public CompositeFilter compositeFilter() {
    var filter = new CompositeFilter();
    filter.setFilters(List.of(httpFilter()));
    return filter;
  }

  public HttpLogger httpFilter() {
    return HttpLogger.builder().build();
  }
}
