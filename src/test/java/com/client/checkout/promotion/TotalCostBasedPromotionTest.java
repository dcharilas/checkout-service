package com.client.checkout.promotion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;


import com.client.checkout.config.promotion.CostDiscountProperties;
import com.client.checkout.model.CheckoutItem;
import com.client.checkout.model.CheckoutResponse;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Created by Dimitris Charilas on 07/10/21.
 */
@ExtendWith(SpringExtension.class)
public class TotalCostBasedPromotionTest {

    private TotalCostBasedPromotion totalCostBasedPromotion;

    @MockBean
    private CostDiscountProperties properties;

    @BeforeEach
    void setUp() {
        when(properties.getDiscounts()).thenReturn(List.of(
            CostDiscountProperties.CostPromotionConfig.builder().minimumCost(75d).discountPercentage(10d).build()
        ));
        when(properties.isActive()).thenReturn(true);

        totalCostBasedPromotion = new TotalCostBasedPromotion(properties);
    }

    @Test
    @DisplayName("Test Discount applied after taking into account quantity")
    void applyDiscountConsideringQuantity() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setPrice(BigDecimal.TEN);
        item.setQuantity(10);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        totalCostBasedPromotion.apply(response);
        assertNotNull(response.getAppliedDiscounts());
        assertEquals(0, response.getDiscountPercentage().compareTo(BigDecimal.TEN));
    }

    @Test
    @DisplayName("Test Discount added to already existing discount")
    void applyDiscountAdded() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setPrice(BigDecimal.TEN);
        item.setQuantity(10);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).discountPercentage(BigDecimal.TEN).build();

        //act && assert
        totalCostBasedPromotion.apply(response);
        assertNotNull(response.getAppliedDiscounts());
        assertEquals(0, response.getDiscountPercentage().compareTo(BigDecimal.valueOf(20)));
    }

    @Test
    @DisplayName("Test Discount not applied")
    void discountNotApplied() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setPrice(BigDecimal.TEN);
        item.setQuantity(1);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        totalCostBasedPromotion.apply(response);
        assertNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.ZERO,response.getDiscountPercentage());
    }

    @Test
    @DisplayName("Test Discount not applied when at exact limit")
    void discountNotAppliedExactLimit() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setPrice(BigDecimal.valueOf(75));
        item.setQuantity(1);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        totalCostBasedPromotion.apply(response);
        assertNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.ZERO,response.getDiscountPercentage());
    }

    @Test
    @DisplayName("Test promotion skipped as inactive")
    void skipInactivePromotion() {
        //arrange
        when(properties.isActive()).thenReturn(false);
        totalCostBasedPromotion = new TotalCostBasedPromotion(properties);

        CheckoutItem item = new CheckoutItem();
        item.setPrice(BigDecimal.TEN);
        item.setQuantity(10);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        totalCostBasedPromotion.apply(response);
        assertNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.ZERO,response.getDiscountPercentage());
    }

    @Test
    @DisplayName("Test that proper discount is selected among list of discounts")
    void multipleDiscounts() {
        //arrange
        when(properties.getDiscounts()).thenReturn(List.of(
            CostDiscountProperties.CostPromotionConfig.builder().minimumCost(75d).discountPercentage(10d).build(),
            CostDiscountProperties.CostPromotionConfig.builder().minimumCost(100d).discountPercentage(15d).build(),
            CostDiscountProperties.CostPromotionConfig.builder().minimumCost(25d).discountPercentage(5d).build()
        ));
        when(properties.isActive()).thenReturn(true);

        totalCostBasedPromotion = new TotalCostBasedPromotion(properties);

        CheckoutItem item = new CheckoutItem();
        item.setPrice(BigDecimal.TEN);
        item.setQuantity(15);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        totalCostBasedPromotion.apply(response);
        assertNotNull(response.getAppliedDiscounts());
        assertEquals(0, response.getDiscountPercentage().compareTo(BigDecimal.valueOf(15)));
    }
}
