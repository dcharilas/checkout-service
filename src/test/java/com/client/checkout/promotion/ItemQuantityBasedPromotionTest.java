package com.client.checkout.promotion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;


import com.client.checkout.config.promotion.QuantityDiscountProperties;
import com.client.checkout.model.CheckoutItem;
import com.client.checkout.model.CheckoutResponse;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Created by Dimitris Charilas on 07/10/21.
 */
@ExtendWith(SpringExtension.class)
public class ItemQuantityBasedPromotionTest {

    private ItemQuantityBasedPromotion itemQuantityBasedPromotion;

    @MockBean
    private QuantityDiscountProperties properties;

    @BeforeEach
    void setUp() {
        when(properties.getDiscounts()).thenReturn(List.of(
            QuantityDiscountProperties.QuantityPromotionConfig.builder().productId("0001").minimumQuantity(2).discount(1.96d).build()
        ));
        when(properties.isActive()).thenReturn(true);

        itemQuantityBasedPromotion = new ItemQuantityBasedPromotion(properties);
    }

    @Test
    @DisplayName("Test Discount applied")
    void applyDiscount() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setId("0001");
        item.setQuantity(2);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        itemQuantityBasedPromotion.apply(response);
        assertNotNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.valueOf(3.92d),response.getAbsoluteDiscount());
    }

    @Test
    @DisplayName("Test Discount added to already existing discount")
    void applyDiscountAdded() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setId("0001");
        item.setQuantity(2);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).absoluteDiscount(BigDecimal.TEN).build();

        //act && assert
        itemQuantityBasedPromotion.apply(response);
        assertNotNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.valueOf(13.92d),response.getAbsoluteDiscount());
    }

    @Test
    @DisplayName("Test Discount not applied due to quantity")
    void discountNotAppliedWrongQuantity() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setId("0001");
        item.setQuantity(1);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        itemQuantityBasedPromotion.apply(response);
        assertNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.ZERO,response.getDiscountPercentage());
    }

    @Test
    @DisplayName("Test Discount not applied due to product id")
    void discountNotAppliedWrongProductId() {
        //arrange
        CheckoutItem item = new CheckoutItem();
        item.setId("0002");
        item.setQuantity(10);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        itemQuantityBasedPromotion.apply(response);
        assertNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.ZERO,response.getDiscountPercentage());
    }

    @Test
    @DisplayName("Test promotion skipped as inactive")
    void skipInactivePromotion() {
        //arrange
        when(properties.isActive()).thenReturn(false);
        itemQuantityBasedPromotion = new ItemQuantityBasedPromotion(properties);

        CheckoutItem item = new CheckoutItem();
        item.setId("0001");
        item.setQuantity(2);
        CheckoutResponse response = CheckoutResponse.builder().items(List.of(item)).build();

        //act && assert
        itemQuantityBasedPromotion.apply(response);
        assertNull(response.getAppliedDiscounts());
        assertEquals(BigDecimal.ZERO,response.getAbsoluteDiscount());
    }
}
