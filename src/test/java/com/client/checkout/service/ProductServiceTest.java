package com.client.checkout.service;

import com.client.checkout.domain.product.Product;
import com.client.checkout.exception.InvalidProductException;
import com.client.checkout.model.CheckoutItem;
import com.client.checkout.model.CheckoutResponse;
import com.client.checkout.repository.ProductRepository;
import com.client.checkout.service.CheckoutService;
import com.client.checkout.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.when;

/**
 * Created by Dimitris Charilas on 09/10/21.
 */
@ExtendWith(SpringExtension.class)
public class ProductServiceTest {

	private ProductService productService;

	@MockBean
	private ProductRepository productRepository;

	@BeforeEach
	void setUp() {
		when(productRepository.getProductDefinitions(anySet())).thenReturn(
				List.of(Product.builder().id("0001").name("Water Bottle").price(BigDecimal.valueOf(24.95)).build())
		);

		productService = new ProductService(productRepository);
	}

	@Test
	void getProductDefinitionsFound() {
		//arrange
		Set<String> ids = Set.of("0001");

		//act && assert
		Map<String,Product> products = productService.getProductDefinitions(ids);
		assertEquals(products.keySet().size(),1);
		assertEquals(products.keySet().stream().findFirst().get(),"0001");
		assertEquals(products.get("0001").getId(),"0001");
	}

	@Test
	void getProductDefinitionsNotFound() {
		//arrange
		Set<String> ids = Set.of("0002");

		//act && assert
		assertThrows(InvalidProductException.class, () -> productService.getProductDefinitions(ids));
	}
}
