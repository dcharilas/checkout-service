package com.client.checkout.service;

import com.client.checkout.model.CheckoutItem;
import com.client.checkout.model.CheckoutResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Dimitris Charilas on 09/10/21.
 */
public class CheckoutServiceTest {

	private CheckoutService checkoutService;

	@MockBean
	private ProductService productService;

	@BeforeEach
	void setUp() {
		checkoutService = new CheckoutService(productService, new LinkedList<>());
	}

	@Test
	void calculateTotalCost() {
		//arrange
		CheckoutItem item = new CheckoutItem();
		item.setQuantity(1);
		item.setPrice(BigDecimal.TEN);
		CheckoutItem item2 = new CheckoutItem();
		item2.setQuantity(3);
		item2.setPrice(BigDecimal.ONE);
		CheckoutResponse response = CheckoutResponse.builder()
				.items(List.of(item,item2))
				.discountPercentage(BigDecimal.TEN)
				.absoluteDiscount(BigDecimal.ONE)
				.build();

		//act && assert
		BigDecimal cost = ReflectionTestUtils.invokeMethod(checkoutService, "calculateTotalCost", response);
		assertEquals(cost.toPlainString(), "10.80");
	}
}
