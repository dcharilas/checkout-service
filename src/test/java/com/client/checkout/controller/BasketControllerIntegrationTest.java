package com.client.checkout.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import com.client.checkout.model.basket.Basket;
import com.client.checkout.model.basket.BasketItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import lombok.SneakyThrows;

import java.util.List;

/**
 * Created by Dimitris Charilas on 07/10/21.
 */
@SpringBootTest
@AutoConfigureMockMvc
public class BasketControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    public ObjectMapper objectMapper;

    @Nested
    @DisplayName("Tests for Cost Calculation")
    class CostCalculationTests {

        @Test
        @DisplayName("Test Checkout of Items: 0001,0001,0002,0003")
        void checkoutBothTotalPriceAndQuantity() throws Exception {
            //arrange
            Basket basket = Basket.builder()
                .items(List.of(
                    BasketItem.builder().id("0001").quantity(2).build(),
                    BasketItem.builder().id("0002").quantity(1).build(),
                    BasketItem.builder().id("0003").quantity(1).build()
                ))
                .build();

            //act && assert
            assertEquals(getTotalCost(basket), "103.47");
        }

        @Test
        @DisplayName("Test Checkout of Items: 0001,0001,0002,0003")
        void checkoutBothTotalPriceAndQuantitySplitItems() throws Exception {
            //arrange
            Basket basket = Basket.builder()
                .items(List.of(
                    BasketItem.builder().id("0001").quantity(1).build(),
                    BasketItem.builder().id("0001").quantity(1).build(),
                    BasketItem.builder().id("0002").quantity(1).build(),
                    BasketItem.builder().id("0003").quantity(1).build()
                ))
                .build();

            //act && assert
            assertEquals(getTotalCost(basket), "103.47");
        }

        @Test
        @DisplayName("Test Checkout of Items: 0001,0001,0001")
        void checkoutManyTimesSameProduct() throws Exception {
            //arrange
            Basket basket = Basket.builder()
                .items(List.of(
                    BasketItem.builder().id("0001").quantity(3).build()))
                .build();

            //act && assert
            assertEquals(getTotalCost(basket), "68.97");
        }

        @Test
        @DisplayName("Test Checkout of Items: 0002,0002,0003")
        void checkoutBothTotalPriceAndQuantity2() throws Exception {
            //arrange
            Basket basket = Basket.builder()
                .items(List.of(
                    BasketItem.builder().id("0002").quantity(2).build(),
                    BasketItem.builder().id("0003").quantity(1).build()
                ))
                .build();

            //act && assert
            assertEquals(getTotalCost(basket), "120.59");
        }

        @SneakyThrows
        private String getTotalCost(Basket basket) {
            MvcResult result = submitBasket(basket);
            return "" + JsonPath.read(result.getResponse().getContentAsString(), "$.data.totalCost");
        }

    }

    @Nested
    @DisplayName("Tests for Basket validation")
    class BasketValidationTests {

        @Test
        @DisplayName("Test Checkout of Empty Basket")
        void checkoutEmptyBasketFail() throws Exception {
            //arrange
            Basket basket = Basket.builder().build();

            //act && assert
            mockMvc.perform(post("/basket/checkout")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(basket))
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        }

        @Test
        @DisplayName("Test Checkout of negative quantity")
        void checkoutNegativeQuantityFail() throws Exception {
            //arrange
            Basket basket = Basket.builder()
                .items(List.of(
                    BasketItem.builder().id("0001").quantity(-2).build()
                ))
                .build();

            //act && assert
            mockMvc.perform(post("/basket/checkout")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(basket))
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        }

        @Test
        @DisplayName("Test Checkout of missing product id")
        void checkoutMissingProductIdFail() throws Exception {
            //arrange
            Basket basket = Basket.builder()
                .items(List.of(
                    BasketItem.builder().quantity(-1).build()
                ))
                .build();

            //act && assert
            mockMvc.perform(post("/basket/checkout")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(basket))
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        }

        @Test
        @DisplayName("Test Checkout of Invalid Product")
        void checkoutInvalidProductFail() throws Exception {
            //arrange
            Basket basket = Basket.builder()
                .items(List.of(
                    BasketItem.builder().id("0005").quantity(2).build()
                ))
                .build();

            //act && assert
            mockMvc.perform(post("/basket/checkout")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(basket))
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
        }
    }

    @SneakyThrows
    private MvcResult submitBasket(Basket basket) {
        return mockMvc.perform(post("/basket/checkout")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(basket))
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    }
}
